all:
	$(MAKE) parser
	$(MAKE) scanner	
	g++ -w parser.tab.c lex.yy.c compiler/main.cpp -o sintatico 

scanner:
	flex compiler/scanner.l

parser:
	bison -d compiler/parser.y