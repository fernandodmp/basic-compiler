#pragma once
#include<vector>
#include<string>
#include<iostream>

namespace compiler {

    class Expression{
        public:
            Expression(){};
            virtual std::string toAST(){};
    };

    class Statement{
        public:
            virtual std::string toAST(){};
            virtual bool searchReturn(){};
    };

    
    class Factor{
        public:
            virtual std::string toAST(){};
    };


    class Term{
        public:
            virtual std::string toAST(){};
    };

    class TermFactor : public Term{ 
        public:
            Factor &factor;
        
        public:
            TermFactor(Factor &f) : factor(f) {};
            std::string toAST(){
                return factor.toAST();
            };
    };

    class TermPlus: public Term{
        public:
            Factor &factor;
            Term &term;
            std::string operand;

        public:
            TermPlus(Factor &f, Term &t, std::string o) : factor(f), term(t), operand(o) {};
            virtual std::string toAST(){
                std::string tree = " \n[" + operand;
                tree += term.toAST();
                tree += factor.toAST();
                tree += "]";
                return tree;
            };
    };
    

    class Declaration{
        public:
            std::string type;
            std::string name;

        public:
            Declaration(std::string t, std::string n): type(t), name(n){};
            virtual std::string toAST(){};
    };

    class Param{
        public:
            std::string type;
            std::string name;
            bool isArray;

        public:
            Param(std::string t, std::string n, bool a) : type(t), name(n), isArray(a) {};
            std::string toAST(){
                if(type == "void"){
                    return NULL;
                } else {
                    std::string tree;
                    tree = " \n[param ";
                    tree += "[" + type + "]";
                    tree += " [" + name + "]";
                    if(isArray){
                        tree += " [\\ [\\]]";
                    }
                    tree += "]";
                    return tree;
                }
            };
    };

    class VarDeclaration : public Declaration{
        public:
            int size;

        public:
            VarDeclaration(std::string t, std::string n) : Declaration(t, n), size(-1){}
            VarDeclaration(std::string t, std::string n, int s) : Declaration(t, n), size(s) {}
            std::string toAST(){
                
                std::string tree = " \n[var-declaration";
                tree += " ["+type+"]";
                tree+= " ["+ name + "]";
                if(size != -1){
                    tree += " [" + std::to_string(size) + "]";
                }
                tree+= "]";
                return tree;
            };
    };

    class CompoundStatement : public Statement{
        public:
            std::vector<VarDeclaration *> localDeclarations;
            std::vector<Statement *> statements;

        public:
            CompoundStatement(std::vector<VarDeclaration *> ld, std::vector<Statement *> s) :
                localDeclarations(ld), statements(s) {};
            std::string toAST(){               
                std::string tree = " \n[compound-stmt";
                for(VarDeclaration *declaration: localDeclarations){
                    tree += declaration->toAST();
                }
                for(Statement *statement: statements){
                    tree += statement->toAST();
                }
                tree +="\n]";
                return tree;
            };

            bool searchReturn(){
                for(Statement *s: statements){
                    if(s->searchReturn()){
                        return true;
                    }
                }
                return false;
            }
    };

    
    class Var{
        public:
            virtual std::string toAST(){};
    };

    class VarInt : public Var{
        public:
            std::string name;

        public:
            VarInt(std::string n) : name(n){};
            std::string toAST(){
                std::string tree;
                tree=" [var";
                tree+=" [" + name + "]]";
                return tree;
            };

    };

    class VarArray : public Var{
        public:
            std::string name;
            Expression &expression;

        public:
            VarArray(std::string n, Expression &e) : name(n), expression(e) {};
            std::string toAST(){
                std::string tree;
                tree=" [var";
                tree+=" [" + name + "]";
                tree += expression.toAST();
                tree += "]";
                return tree;
            };
    };

    class Program{
        public:
            std::vector<Declaration *> declarations;
        public:  
            Program(std::vector<Declaration *> d) : declarations(d) {}

            std::string toAST(){
                std::string tree = "[program";
                for(Declaration *declaration: this->declarations){
                    tree += declaration->toAST();
                }
                tree += "\n]";
                return tree;
            };
    };


    class ExpressionStatement : public Statement{
        public:
            ExpressionStatement(){};
            std::string toAST(){
                return " \n[;]";
            }
            bool searchReturn(){
                return false;
            }
    };

    class ExpressionStatementWExpression : public Statement{
        public: 
            Expression &expression;

        public:
            ExpressionStatementWExpression(Expression &e) : expression(e) {};
            std::string toAST(){
                return expression.toAST();
            }
            bool searchReturn(){
                return false;
            }
    };

    class SelectionStatement : public Statement{
        public:
            Expression &expression;
            Statement &then;

        public:
            SelectionStatement(Expression &e, Statement &t) : expression(e), then(t) {};
            std::string toAST(){
                std::string tree;
                tree =" \n[selection-stmt";
                tree += expression.toAST();
                tree += then.toAST();
                tree += "\n]";
                return tree;
            };
            bool searchReturn(){
                return then.searchReturn();
            }
    };

    class SelectionStatementElse : public SelectionStatement{
        public:
            Statement &other;

        public:
            SelectionStatementElse(Expression &e, Statement &t, Statement &o) : SelectionStatement(e, t), other(o) {};
            std::string toAST(){
                std::string tree;
                tree =" \n[selection-stmt";
                tree += expression.toAST();
                tree += then.toAST();
                tree += other.toAST();
                tree += "\n]";
                return tree;
            };

            bool searchReturn(){
                if(then.searchReturn() || other.searchReturn()){
                    return true;
                }
                return false;
            }
    };

    class IterationStatement : public Statement{
        public:
            Expression &expression;
            Statement &statement;

        public:
            IterationStatement(Expression &e, Statement &t) : expression(e), statement(t) {};
            std::string toAST(){
                std::string tree = " \n[iteration-stmt";
                tree += expression.toAST();
                tree += statement.toAST();
                tree += "\n]";
                return tree;
            };
            
            bool searchReturn(){
                return statement.searchReturn();
            }
    };

    class ReturnStatement : public Statement{
        public:
            ReturnStatement(){};
            std::string toAST(){
                return " \n[return-stmt]";
            };
            bool searchReturn(){
                return false;
            }
    };

    class ReturnStatementWValue : public ReturnStatement{
        public:
            Expression &expression;
        
        public:
            ReturnStatementWValue(Expression &e) : expression(e) {};
            std::string toAST(){
                std::string tree = " \n[return-stmt";
                tree += expression.toAST();
                tree += "]";
                return tree;
            };
            bool searchReturn(){
                return true;
            }
    };

    class FunDeclaration : public Declaration{
        public:
            std::vector<Param *> params;
            CompoundStatement &compoundStmt;
        public:
            FunDeclaration(std::string t, std::string n, std::vector<Param *> p, CompoundStatement &b) 
                : Declaration(t, n), params(p), compoundStmt(b) {};

            
            std::string toAST(){
                std::string tree;
                tree = " \n[fun-declaration";
                tree += " \n["+ type +"]";
                tree += " \n[" + name  + "]";
                tree += " \n[params";
                if(params.size() > 0){
                    for(Param *param: params){
                        tree += param->toAST();
                    };
                }
                tree += "]";
                tree +=compoundStmt.toAST();
                tree += "\n]";
                return tree;
            };

            bool searchReturn(){
                bool found = false;
                for(Statement *s : compoundStmt.statements){
                    if(s->searchReturn()){
                        found = true;
                    }
                }               
                if(type == "void" && found){
                    return true;
                }
                if(type == "int" && !found){
                    return true;
                }
                return false;
            }

    };

    class Assignment : public Expression{
        public:
            Var &var;
            Expression &expression;
        public:
            Assignment(Var &v, Expression &e) : expression(e), var(v) {};
            std::string toAST(){
                std::string tree;
                tree+=" \n[=";
                tree += var.toAST();
                tree += expression.toAST();
                tree += "]";
                return tree;
            };
    };

    class Addition : public Expression{
        public:
            Term &term;

        public:
            Addition(Term &t) : term(t) {};
            std::string toAST(){
                return term.toAST();
            };
    };

    class AdditionPlus : public Addition{
        public:
            Addition &addition;
            std::string operand;

        public:
            AdditionPlus(Term &t, Addition &a, std::string o) : Addition(t), addition(a), operand(o) {};
            std::string toAST(){
                std::string tree;
                tree+=" \n[" + operand;
                tree+=addition.toAST();
                tree+=term.toAST();
                tree+="]";
                return tree;
            };
    };

    class Comparisson : public Expression{
        public:
            Addition &addition;

        public:
            Comparisson(Addition &a) : addition(a) {};
            std::string toAST(){
                return addition.toAST();
            };

    };

    class Comparisson2Additions : public Comparisson{
        public:
            std::string operand;
            Addition &addition2;
        public:
            Comparisson2Additions(Addition &a, Addition &a2, std::string r) : Comparisson(a), operand(r), addition2(a2) {};
            std::string toAST(){
                std::string tree = " \n[" + operand;
                tree += addition.toAST();
                tree += addition2.toAST();
                tree += "]";
                return tree;
            };
    };

    class LogicAnd : public Expression{
        public:
            virtual std::string toAST(){};
    };

    
    class LogicAndComparisson : public LogicAnd{
        public:
            Comparisson &comparisson;

        public:
            LogicAndComparisson(Comparisson &c) : comparisson(c) {};
            std::string toAST(){
                return comparisson.toAST();
            };
    };

    class LogicAndWAnd : public LogicAnd{
        public:
            Comparisson &comparisson;
            LogicAnd &andexpression;

        public:
            LogicAndWAnd( LogicAnd &a, Comparisson &c) : andexpression(a), comparisson(c) {};
            std::string toAST(){
                std::string tree = " [&&";
                tree+=andexpression.toAST();
                tree+=comparisson.toAST();
                tree+="]";
                return tree;
            };
    };

    class LogicOr : public Expression{
        public:
            virtual std::string toAST(){};
    };

    class LogicOrAnd : public LogicOr{
        public:
            LogicAnd &andexpression;

        public:
            LogicOrAnd(LogicAnd &a) : andexpression(a) {};
            std::string toAST(){
                return andexpression.toAST();
            };
    };

    class LogicOrWOr : public LogicOr{
        public:
            LogicOr &orexpression;
            LogicAnd &andexpression;


        public:
            LogicOrWOr(LogicOr &o, LogicAnd &a) : orexpression(o), andexpression(a) {};
            std::string toAST(){
                std::string tree = " [||";
                tree+=orexpression.toAST();
                tree+=andexpression.toAST();              
                tree+="]";
                return tree;
            };
    };
    
    class UnaryFactor : public Factor{
        public:
            std::string operand;
            Factor &factor;
        
        public:
            UnaryFactor(Factor &f, std::string o) : factor(f), operand(o) {};
            std::string toAST(){
                std::string tree = " [" + operand;
                tree += factor.toAST();
                tree += "]";
                return tree;
            };

    };

    class ExpressionFactor : public Factor{
        public:
            Expression &expression;
        public:
            ExpressionFactor(Expression &e) : expression(e) {};
            std::string toAST(){
                return expression.toAST();
            };
    };

    class VarFactor : public Factor{
        public:
            Var &var;
        public:
            VarFactor(Var &v): var(v) {};
            std::string toAST(){
                return var.toAST();
            };
    };

    class NumFactor : public Factor{
        public:
            int num;
        public:
            NumFactor(int n): num(n) {};
            std::string toAST(){
                std::string tree;
                tree+= " ["+ std::to_string(num)+"]";
                return tree;
            };
    };

    class Arg{
        public:
            std::vector<Expression *> argList;
        public:
            Arg(std::vector<Expression *> a) : argList(a) {};
            std::string toAST(){
                std::string tree = " \n[args";
                for(Expression *expression: argList){
                    tree+= expression->toAST();
                }
                tree += "]";
                return tree;
            };  
    };

    class CallFactor : public Factor{
        public:
            std::string name;
            Arg &args;
        public:
            CallFactor(std::string n, Arg &a) : name(n), args(a) {};
            std::string toAST(){
                std::string tree = " \n[call";
                tree += " \n[" + name + "]";
                tree += args.toAST();
                tree += "\n]";
                return tree;
            };  
    };

    
}