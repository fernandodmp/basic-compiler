%option noyywrap
%option yylineno
%{
    #include <stdio.h>
    #include <string>
    #include "parser.tab.h"
%}


ws [ \t] 

linebreak \n 

letter [a-zA-Z]

digit [0-9]

id {letter}({letter}|{digit})*

num ({digit})+

id_error {num}{id}

commenterror [/][*]

comment [/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]


%%
"else" {return ELSE;}
"if"    {return IF;}
"int"   {yylval.stringval = new std::string(yytext, yyleng); return INT;}
"return" {return RETURN;}
"void" { yylval.stringval = new std::string(yytext, yyleng); return VOID;}
"while" {return WHILE;}
{comment} ;
{ws} ;
{linebreak} ;
{id} {yylval.stringval = new std::string(yytext, yyleng); return ID;}
{num} { yylval.intval = atoi(yytext); return NUM;}

"+" {yylval.stringval = new std::string(yytext, yyleng); return ADD_OP;} 
"-" {yylval.stringval = new std::string(yytext, yyleng); return SUB_OP;} 
"*" {yylval.stringval = new std::string(yytext, yyleng); return MUL_OP;} 
"/" {yylval.stringval = new std::string(yytext, yyleng); return DIV_OP;} 
"<" {yylval.stringval = new std::string(yytext, yyleng); return L_OP;} 
"<=" {yylval.stringval = new std::string(yytext, yyleng); return LE_OP;}   
">" {yylval.stringval = new std::string(yytext, yyleng); return G_OP;}   
">=" {yylval.stringval = new std::string(yytext, yyleng); return GE_OP;}   
"==" {yylval.stringval = new std::string(yytext, yyleng); return EQ_OP;}   
"!=" {yylval.stringval = new std::string(yytext, yyleng); return NE_OP;}   
"=" {return ASSIGN_OP;}   
";" {return S_COLON;}   
"," {return COLON;}   
"(" {return L_PAR;}   
")" {return R_PAR;}   
"[" {return L_BRACK;}   
"]" {return R_BRACK;}   
"{" {return L_BRACE;}      
"}" {return R_BRACE;} 


. {return 1;};
