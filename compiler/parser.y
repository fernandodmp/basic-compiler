%code requires{
    #include <vector>
    #include <string>
    #include "compiler/TreeClasses.hpp"   
    using namespace compiler;
}

%{
    #include "compiler/TreeClasses.hpp"   
    #include <stdio.h>
    #include <math.h>
    #include <stdlib.h>
    #include <iostream>
    int yylex(void);
    void yyerror(char const *);    
    compiler::Program *program = NULL;
%}


//%define api.value.type {compiler::SyntaxTree *}

%union {
    int intval;
    std::string  *stringval;
    Param *parameter;
    Var *v;
    Program *p;
    Declaration *dc;
    Statement *stm;
    Expression *ex;
    Factor *f;
    Term *t;
    Addition *add;
    VarDeclaration *vd;
    CompoundStatement *compound;
    std::vector<VarDeclaration *> *lds;
    std::vector<Declaration *> *dcs;
    std::vector<Param *> *parameters;
    std::vector<Expression *> *exs;
    std::vector<Statement *> *stms;
    Arg *arg;
    Comparisson *cmp;
}

%token <stringval> ID INT VOID ADD_OP MUL_OP DIV_OP SUB_OP LE_OP GE_OP EQ_OP NE_OP G_OP L_OP AND_OP OR_OP NEG_OP
%token <intval> NUM
%token ASSIGN_OP
%token IF ELSE WHILE RETURN
%token L_PAR R_PAR L_BRACK R_BRACK L_BRACE R_BRACE
%token COLON S_COLON

%type <p> program;
%type <dc> declaration fun_declaration
%type <dcs> declaration_list 
%type <stringval> type_specifier mulop addop relop
%type <parameters> param_list params
%type <parameter> param
%type <stm> iteration_stmt selection_stmt expression_stmt return_stmt statement
%type <stms> statement_list
%type <ex> expression 
%type <v> var
%type <arg>args
%type <exs> arg_list
%type <f> factor call
%type<t> term
%type <add> additive_expression
%type <lds> local_declarations
%type <vd> var_declaration
%type <compound> compound_stmt
%type <cmp>compare_expression
%nonassoc IFX
%nonassoc ELSE

%start program
%%

program: 
    declaration_list 
    {//puts("Creating Program"); 
    $$ = new Program(*$1); program = $$;};

declaration_list: 
    declaration_list declaration 
        {//puts("Pushing declaration");
        $1->push_back($2);}

    | declaration 
        {//puts("Creating declaration-list"); 
        $$ = new std::vector<Declaration *>; $$->push_back($1);};

declaration:
    var_declaration { $$ = (Declaration *) $1;}
    | fun_declaration;

var_declaration:
    type_specifier ID S_COLON 
        {//puts("Creating var-declaration");
        if(*$1 == "void"){
            yyerror("Var declaration with void type.");
        }; 
        $$ = new VarDeclaration(*$1, *$2); }

    | type_specifier ID L_BRACK NUM R_BRACK S_COLON
        {//puts("Creating var-declaration");
        if(*$1 == "void"){
            yyerror("Var declaration with void type.");
        }; 
        $$ = new VarDeclaration(*$1, *$2, $4);};

type_specifier: INT { $$ = $1;}| VOID {$$ = $1;};

fun_declaration: 
    type_specifier ID L_PAR params R_PAR compound_stmt 
        {//puts("Creating fun-declaration");
         FunDeclaration *dec = new FunDeclaration(*$1, *$2, *$4, *$6); 
         bool returnProblem = dec->searchReturn();
         if(returnProblem && *$1 == "void"){
             yyerror("Void function returning int");
         }
         if(returnProblem && *$1 == "int"){
             yyerror("Int function returning void");
         }
         $$ = dec;};

params:
    param_list {$$ = $1;} 
    | VOID 
        {//puts("Empty Param List"); 
        $$ = new std::vector<Param *>;};

param_list: 
    param_list COLON param 
        {//puts("Pushing to param-list"); 
        $1->push_back($3); $$ = $1;}

    | param 
        {//puts("Pushing to new param-list");
        $$ = new std::vector<Param *>; $$->push_back($1);};

param:
    type_specifier ID 
        {//puts("Creating param");
        if(*$1 == "void"){
            yyerror("Param specifier with void type.");
        }; 
        $$ = new Param(*$1, *$2, false);}

    | type_specifier ID L_BRACK R_BRACK 
        {//puts("Creating array param");
        if(*$1 == "void"){
            yyerror("Param specifier with void type.");
        }; 
        $$ = new Param(*$1, *$2, true);};

compound_stmt: 
    L_BRACE local_declarations statement_list R_BRACE 
        {//puts("Creating compound-statement"); 
        $$ = new CompoundStatement(*$2, *$3);};

local_declarations: 
    local_declarations var_declaration 
        {//puts("Pushing to local-declarations"); 
        $1->push_back($2); $$ = $1;}

    | %empty 
        {//puts("New local-declarations"); 
        $$ = new std::vector<VarDeclaration *>;};

statement_list:
    statement_list statement 
        {//puts("Pushing to statement-list"); 
        $1->push_back($2); $$ = $1;}

    | %empty 
        {//puts("New statement-list"); 
        $$ = new std::vector<Statement *>;};

statement:
    expression_stmt {$$ = (Statement *) $1;}
    | compound_stmt {$$ = (Statement *) $1;}
    | selection_stmt {$$ = (Statement *) $1;}
    | iteration_stmt {$$ = (Statement *) $1;}
    | return_stmt {$$ = (Statement *) $1;};

expression_stmt:
    expression S_COLON 
        {//puts("New expression-stmt with expression");
        $$ = new ExpressionStatementWExpression(*$1);}

    | S_COLON 
        {//puts("New expression-stmt"); 
        $$ = new ExpressionStatement();};

selection_stmt:
    IF L_PAR expression R_PAR statement %prec IFX 
        {//puts("New selection-stmt");
        $$ = new SelectionStatement(*$3, *$5);}

    | IF L_PAR expression R_PAR statement ELSE statement 
        {//puts("New selection-stmt with else");
        $$ = new SelectionStatementElse(*$3, *$5, *$7);};

iteration_stmt: 
    WHILE L_PAR expression R_PAR statement 
        {//puts("New iteration-stmt");
        $$ = new IterationStatement(*$3, *$5);};

return_stmt:
    RETURN S_COLON 
        {//puts("New return-stmt");
        $$ = new ReturnStatement();}

    |RETURN expression S_COLON 
        {//puts("New selection-stmt with expression");
        $$ = new ReturnStatementWValue(*$2);};

expression:
    var ASSIGN_OP expression 
        {//puts("New assignment");
        $$ = new Assignment(*$1, *$3);}
    | compare_expression 
        {$$ = $1;};

var:
    ID 
        {//puts("New var");
        $$ = new VarInt(*$1);}

    |ID L_BRACK expression R_BRACK 
        {//puts("New array-var");
        $$ = new VarArray(*$1, *$3);};

compare_expression:
    additive_expression relop additive_expression 
        {//puts("New compare-expressions");
        $$ = new Comparisson2Additions(*$1, *$3, *$2);}

    | additive_expression 
        {//puts("New addition-comparison"); 
        $$ = new Comparisson(*$1);};

relop: LE_OP | L_OP | G_OP | GE_OP | EQ_OP | NE_OP;

additive_expression:
    additive_expression addop term 
        {//puts("New additionplus");
        $$ = new AdditionPlus(*$3, *$1, *$2);}

    | term 
        {//puts("New Addition"); 
        $$ = new Addition(*$1);};

addop: ADD_OP {$$ = $1;}| SUB_OP {$$ = $1;};

term:
    term mulop factor 
        {//puts("New term"); 
        $$ = new TermPlus(*$3, *$1, *$2);}

    |factor 
        {//puts("New term-factor");
        $$ = new TermFactor(*$1);};

mulop: MUL_OP {$$ = $1;}| DIV_OP {$$ = $1;};

factor:
    L_PAR expression R_PAR 
        {//puts("New parenthesis expression");
        $$ = new ExpressionFactor(*$2);}
    | var 
        {//puts("New var-factor");
        $$ = new VarFactor(*$1);}
    | call {$$ = $1;}
    | NUM 
        {//puts("New num-factor");
        $$ = new NumFactor($1);};

call: ID L_PAR args R_PAR 
    {//puts("New call-factor");
    $$ = new CallFactor(*$1, *$3);};

args: 
    arg_list 
        {//puts("New arg-list");
        $$ = new Arg(*$1);}

    |%empty 
        {//puts("New empty arg-list");
        std::vector<Expression *> ex; 
        $$ = new Arg(ex);}; 

arg_list:
    arg_list COLON expression 
        {//puts("Pushing to arg-list");
        $1->push_back($3); $$ = $1;}

    | expression 
        {//puts("Pushing to new arg-list");
        $$ = new std::vector<Expression *>; 
        $$->push_back($1);};


%%

void yyerror(char const *x){
    printf("Error: %s\n", x);
    exit(1);
}