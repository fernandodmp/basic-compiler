#include <bits/stdc++.h>
#include "TreeClasses.hpp"
#include "SymbolTable.hpp"
#include <iostream>

extern compiler::Program *program;
extern std::vector<compiler::FunDeclaration *> funclist;
extern FILE *yyin;
extern FILE *yyout;
extern int yylex();
extern int yyparse();
extern void yyerror();

int main(int argc, char const *argv[]){
    ++argv, --argc;
    if(argc > 1){

        std::vector<compiler::Param *> printP;
        printP.push_back(new compiler::Param("int", "n", false));
        std::vector<compiler::VarDeclaration *> emptyvars;
        std::vector<compiler::Statement *> emptystmt;
        compiler::CompoundStatement *emptycompound = new compiler::CompoundStatement(emptyvars, emptystmt);

        funclist.push_back(new compiler::FunDeclaration("void", "println", printP, *emptycompound));
        printP.pop_back();
        funclist.push_back(new compiler::FunDeclaration("int", "input", printP, *emptycompound));

        yyin = fopen(argv[0], "r");
        if(!yyin){
            return 1;
        }
        std::ofstream out;
        out.open(argv[1], std::ofstream::trunc);
        int result = yyparse();    
        semantics::CheckSymbols();
        if(funclist.back()->name == "main" && funclist.back()->type == "void" && funclist.back()->params.size() == 0){
            out << program->toAST() << std::endl;
        } else {
            yyerror("Program lacks main function");
        }
        fclose(yyin);
        //fclose(yyout);
    } else{
        return 1;
    }
    return 0;
}
 