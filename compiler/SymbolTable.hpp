#include "TreeClasses.hpp"
#include <string>
#include <vector>
extern void yyerror(const char *s);
extern compiler::Program *program;

std::vector<std::vector<compiler::VarDeclaration *> *> symboltable;
std::vector<compiler::FunDeclaration *> funclist;

namespace semantics{

    void CheckSymbols(compiler::CompoundStatement *block);
    void CheckSymbols(compiler::Expression *expr, bool isArg, bool isOperation);

    void CheckSymbols(compiler::Var *var, bool isArg){
        std::vector<std::vector<compiler::VarDeclaration *> *>::reverse_iterator reverse = symboltable.rbegin();

        if(compiler::VarInt *v = dynamic_cast<compiler::VarInt *>(var)){
            for(; reverse != symboltable.rend(); ++reverse){
                for(compiler::VarDeclaration *dec: *(*(reverse))){
                    if(v->name == dec->name){
                        if(dec->size != -1 && !isArg){
                            yyerror("Operations with pointers not allowed");
                        }
                        else{
                            return;
                        }
                    }
                }
            }
            std::string msg = "Var " + v->name + " was not declared";
            yyerror(msg.c_str());
        }

        if(compiler::VarArray *v = dynamic_cast<compiler::VarArray *>(var)){
            for(; reverse != symboltable.rend(); ++reverse){
                for(compiler::VarDeclaration *dec: *(*(reverse))){
                    if(v->name == dec->name){
                        if(dec->size == -1){
                            yyerror("Non-array types can not be indexed");
                        }
                        CheckSymbols(&(v->expression), false, true);
                        return;
                    }
                }
            }
            std::string msg = "Var " + v->name + " was not declared";
            yyerror(msg.c_str());
        }
    }

    void CheckSymbols(compiler::Arg *ar){
        for(compiler::Expression *expr: ar->argList){
            CheckSymbols(expr, true, true);
        }
    }

    void CheckSymbols(compiler::Factor *factor, bool isArg, bool isOperation){
        if(compiler::CallFactor *ass = dynamic_cast<compiler::CallFactor*>(factor)){
            bool found = false;
            for(compiler::FunDeclaration *func: funclist){
                if(func->name == ass->name){
                    if(func->params.size() != ass->args.argList.size()){
                        std::string msg = "Number of parameters doesn't match the number of arguments on function " + func->name;
                        yyerror(msg.c_str());
                    }
                    if(func->type == "void" && isOperation){
                        yyerror("Can not call void function inside operations");
                    }
                    found = true;
                }
            }
            if(!found){
                std::string msg = "Function " + ass->name + " was not declared";
                yyerror(msg.c_str());
            }
            CheckSymbols(&(ass->args));
        }
        if(compiler::ExpressionFactor *ass = dynamic_cast<compiler::ExpressionFactor*>(factor)){
            CheckSymbols(&(ass->expression), false, isOperation);
        }
        if(compiler::VarFactor *ass = dynamic_cast<compiler::VarFactor*>(factor)){
            CheckSymbols(&(ass->var), isArg);
        }

    }

    void CheckSymbols(compiler::Term *term,bool isArg, bool isOperation){
        if(compiler::TermFactor *ass = dynamic_cast<compiler::TermFactor*>(term)){
            CheckSymbols(&(ass->factor), isArg, isOperation);
        }
        if(compiler::TermPlus *ass = dynamic_cast<compiler::TermPlus*>(term)){
            CheckSymbols(&(ass->term), isArg, true);
            CheckSymbols(&(ass->factor), isArg, true);
        }
    }

    void CheckSymbols(compiler::Expression *expr, bool isArg, bool isOperation){
        if(compiler::Assignment *ass = dynamic_cast<compiler::Assignment *>(expr)){
            CheckSymbols(&(ass->var), false);
            CheckSymbols(&(ass->expression), isArg, true);
        }

        if(compiler::Comparisson *cmp = dynamic_cast<compiler::Comparisson *>(expr)){
            if(compiler::Comparisson2Additions *cmpadd = dynamic_cast<compiler::Comparisson2Additions *>(cmp)){
                CheckSymbols(&(cmpadd->addition), isArg, true);
                CheckSymbols(&(cmpadd->addition2), isArg, true);
            }
            else{
                CheckSymbols(&(cmp->addition), isArg, isOperation);
            }
        }
        
        if(compiler::Addition *add = dynamic_cast<compiler::Addition *>(expr)){
            if(compiler::AdditionPlus *addp = dynamic_cast<compiler::AdditionPlus *>(add)){
                CheckSymbols(&(addp->addition), isArg, true);
                CheckSymbols(&(addp->term), isArg, true);
            } else {
                CheckSymbols(&(add->term), isArg, isOperation);
            }
        }
    }

    void CheckSymbols(compiler::Statement *stmt){
        if(compiler::CompoundStatement *block = dynamic_cast<compiler::CompoundStatement *>(stmt)){
            CheckSymbols(block);
        }
        if(compiler::ReturnStatementWValue *return_stmt = dynamic_cast<compiler::ReturnStatementWValue *>(stmt)){
            CheckSymbols(&(return_stmt->expression), false, true);
        }
        if(compiler::ExpressionStatementWExpression *expr = dynamic_cast<compiler::ExpressionStatementWExpression *>(stmt)){
            CheckSymbols(&(expr->expression), false, false);
        }
        if(compiler::IterationStatement *iter = dynamic_cast<compiler::IterationStatement *>(stmt)){
            CheckSymbols(&(iter->expression), false, true);
            CheckSymbols(&(iter->statement));
        }
        if(compiler::SelectionStatement *if_stmt = dynamic_cast<compiler::SelectionStatement *>(stmt)){
            CheckSymbols(&(if_stmt->expression), false, true);
            CheckSymbols(&(if_stmt->then));
            if(compiler::SelectionStatementElse *else_stmt = dynamic_cast<compiler::SelectionStatementElse *>(if_stmt)){
                CheckSymbols(&(else_stmt->other));
            }
        }
    }

    void CheckSymbols(compiler::VarDeclaration *var){
        for(compiler::VarDeclaration *dec: *(symboltable.back())){
            if(var->name == dec->name){
                std::string msg = "Function " + var->name + " was already declared in this scope";
                yyerror(msg.c_str());
            }
        }
        symboltable.back()->push_back(var);   
    }

    void CheckSymbols(compiler::CompoundStatement *block){
        symboltable.push_back(new std::vector<compiler::VarDeclaration *>);
        for(compiler::VarDeclaration *var: block->localDeclarations){
            CheckSymbols(var);
        }
        
        for(compiler::Statement *stmt: block->statements){
            CheckSymbols(stmt);
        }
        symboltable.pop_back();
    }

    void CheckSymbols(compiler::FunDeclaration *func){
        for(compiler::FunDeclaration *f: funclist){
            if(f->name == func->name){
                std::string msg = "Function " + func->name + " was already declared";
                yyerror(msg.c_str());
            }
            std::vector<std::vector<compiler::VarDeclaration *> *>::reverse_iterator reverse = symboltable.rbegin();
            for(; reverse != symboltable.rend(); ++reverse){
                for(compiler::VarDeclaration *dec: *(*(reverse))){
                    if(dec->name == func->name){
                        std::string msg = "Var " + func->name + " redeclared as a function";
                        yyerror(msg.c_str());                    
                    }
                }
            }
        }
        funclist.push_back(func);
        symboltable.push_back(new std::vector<compiler::VarDeclaration *>);
        for(compiler::Param *p: func->params){
            if(p->isArray){
                CheckSymbols(new compiler::VarDeclaration(p->type, p->name, 1));
            } else{
               CheckSymbols(new compiler::VarDeclaration(p->type, p->name));
            }
        }
        CheckSymbols((&func->compoundStmt));
        symboltable.pop_back();
    }

    void CheckSymbols(){
        if(program != NULL){
            symboltable.push_back(new std::vector<compiler::VarDeclaration *>);
            for(compiler::Declaration *dec: program->declarations){
                    if(compiler::FunDeclaration *func = dynamic_cast<compiler::FunDeclaration *>(dec)){
                        CheckSymbols(func);
                    }
                    if(compiler::VarDeclaration *var = dynamic_cast<compiler::VarDeclaration *>(dec)){
                        CheckSymbols(var);
                    }
            }
        }
    }
}