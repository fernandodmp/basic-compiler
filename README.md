# This is a basic compiler I'm doing for a school project

Steps to complete:

-Lexical Scanner - Complete

-Parser - Complete

-Code Generator - On Queue

## How to execute:

You need flex and bison installed

Run make on the makefile directory

./sintatico "Source file path" "Target file path"