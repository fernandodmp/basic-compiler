/*Number of args doesn't match number of parameters*/

int fib(int n) {
	if (n <= 2) {
		return 1;
	} else {
		return fib(n - 1) + fib(n - 2);
	}
	return 0;
}

void main(void) {
	int n;
	n = 5;
	println(fib());
}
